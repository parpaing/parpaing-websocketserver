const ConfigSchema = require('./ConfigSchema');
const WebSocketServer = require('./src/WebSocketServer');

module.exports = {
    Mod: WebSocketServer,
    ConfigSchema
};
