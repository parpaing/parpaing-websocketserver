class WebSocketClient {
    constructor(WSS, id, ws, req) {
        this.WSS = WSS;

        this.ws = ws;
        this.id = id;
        this.req = req;

        this.isAlive = true;
        this.ws.on('pong', () => {
            this.isAlive = true;
        });

        this.ws.on('message', (msg) => {
            const mwReq = {
                client: this,
                data: msg
            };

            this.WSS.mw.onMessage.go(mwReq, () => {});
        });

        this.ws.on('close', () => {
            const mwReq = {
                client: this
            };

            this.WSS.mw.onClose.go(mwReq, () => {});
        });

        this.ws.on('error', (err) => {
            const mwReq = {
                client: this,
                err
            };
            this.WSS.mw.onError.go(mwReq, () => {});
        });

        this.WSS.mw.onConnection.go({ client: this }, () => {});
    }

    send(data) {
        this.ws.send(data);
    }

    sendJSON(data) {
        this.ws.send(JSON.stringify(data));
    }

    setAlive(isAlive) {
        this.isAlive = isAlive;
    }
}

module.exports = WebSocketClient;
