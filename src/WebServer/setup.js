const url = require('url');

const setup = (WS) => {
    if (typeof WS.getMod('WebSocketServer') !== 'undefined') return;

    WS.setMod('WebSocketServer', []);
    WS.Server.server.on('upgrade', (request, socket, head) => {
        const { pathname } = url.parse(request.url);
        const WSSMod = WS.getMod('WebSocketServer');

        if (!WSSMod) {
            socket.destroy();
            return;
        }
        const WSS = WSSMod[pathname];
        if (!WSS) {
            socket.destroy();
            return;
        }

        WSS.handleUpgrade(request, socket, head, (ws) => {
            WSS.emit('connection', ws, request);
        });
    });
};

module.exports = setup;
