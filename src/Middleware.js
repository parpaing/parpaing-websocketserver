/* eslint no-extend-native: ["error", { "exceptions": ["Array"] }] */


class Middleware {
    constructor() {
        if (!Array.prototype.last) {
            Array.prototype.last = function last() {
                return this[this.length - 1];
            };
        }

        if (!Array.prototype.reduceOneRight) {
            Array.prototype.reduceOneRight = function reduceOneRight() {
                return this.slice(0, -1);
            };
        }
    }

    use(fn) {
        this.go = ((stack) => (...args) => stack(...args.reduceOneRight(), () => {
            const next = args.last();
            fn.apply(this, [...args.reduceOneRight(),
                next.bind.apply(next, [null, ...args.reduceOneRight()])]);
        }))(this.go);
    }

    go(...args) {
        const next = args.last();
        next.apply(this, args.reduceOneRight());
    }
}

module.exports = Middleware;
