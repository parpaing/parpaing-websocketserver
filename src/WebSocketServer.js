const fs = require('fs');
const path = require('path');

const ParpaingMod = require('@parpaing/parpaing').Mod;
const WebSocket = require('ws');

const Middleware = require('./Middleware');
const WebSocketClient = require('./WebSocketClient');

const WebServerSetup = require('./WebServer/setup');

class WebSocketServer extends ParpaingMod {
    setup() {
        this.createServer();
        this.clients = [];
        this.client_next_id = 0;

        this.mw = {};
        this.mw.onClose = new Middleware();
        this.mw.onConnection = new Middleware();
        this.mw.onError = new Middleware();
        this.mw.onMessage = new Middleware();

        this.wss.on('connection', (ws, req) => {
            this.clients.push(new WebSocketClient(this, this.client_next_id, ws, req));
            this.client_next_id += 1;
        });

        this.addMiddlewaresFolder(this.root);
    }

    init() {
        setInterval(() => {
            this.wss.clients.forEach((ws) => {
                const client = this.clients.find((c) => c.ws === ws);

                if (client.isAlive === false) {
                    client.ws.terminate();
                    return;
                }

                client.setAlive(false);
                client.ws.ping();
            });
        }, 30000);
    }

    createServer() {
        if (typeof this.deps.WebServer !== 'undefined' && this.deps.WebServer !== null) {
            const serverPath = this.config.path || '/';

            WebServerSetup(this.deps.WebServer);
            this.wss = new WebSocket.Server({ noServer: true });

            const mod = this.deps.WebServer.getMod('WebSocketServer');
            mod[serverPath] = this.wss;
            this.deps.WebServer.setMod('WebSocketServer', mod);
        } else {
            this.wss = new WebSocket.Server({ port: this.config.port });
        }
    }

    addMiddlewaresFolder(rootPath, ...args) {
        this.getOnCloseMW(rootPath, ...args);
        this.getOnConnectionMW(rootPath, ...args);
        this.getOnErrorMW(rootPath, ...args);
        this.getOnMessageMW(rootPath, ...args);
    }

    getMW(event) {
        return this.mw[`on${event}`];
    }

    getOnEventMW(event, root, ...args) {
        const file = path.join(root, `on${event}.js`);

        if (fs.existsSync(file) && fs.statSync(file).isFile()) {
            require(file)(this.getMW(event), ...args);
        }
    }

    getOnCloseMW(root, ...args) {
        this.getOnEventMW('Close', root, ...args);
    }

    getOnConnectionMW(root, ...args) {
        this.getOnEventMW('Connection', root, ...args);
    }

    getOnErrorMW(root, ...args) {
        this.getOnEventMW('Error', root, ...args);
    }

    getOnMessageMW(root, ...args) {
        this.getOnEventMW('Message', root, ...args);
    }
}

module.exports = WebSocketServer;
